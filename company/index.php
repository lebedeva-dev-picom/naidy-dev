<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("История");
?>
<div class="hist_right fl_r">
<?php $APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SUFFIX" => "include/hist_right",
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "page",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>
</div> <!-- /hist_right -->

<div class="hist_left">
<?php $APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SUFFIX" => "include/hist_left",
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "page",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>
</div> <!-- /hist_left -->
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>