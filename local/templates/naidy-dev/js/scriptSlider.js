$(function() {
	$('.slide_list__item .container a').click(function() {

		$('.slide_list__item').removeClass('active');
		$(this).parents('li').addClass('active');

		var img = $(this).data('img');
		$('.index_slider_wrap img').attr('src', img);

		var newName = '';
		function addSpan(item, index) {
			newName += '<span>' + item + '</span>';
		}
		var name = $(this).data('name').split(' ').forEach(addSpan);
		var link = $(this).data('link');
		
		$('.title_wrap .dev_title').html('<a href="' + link + '">' + newName + '</a>');

		return false;
	});
	
	$('.slide_list__item .container a').eq(0).click();
});