$(document).ready(function(){
	$("a[href$='.doc'], a[href$='.docx']").addClass('word');
	$("a[href$='.xls'], a[href$='.xlsx']").addClass('excel');
	$("a[href$='.pdf']").addClass('pdf');
	$("a[href$='.ppt'], a[href$='.pps']").addClass('ppt');
	$("a[href$='.rar'], a[href$='.zip'], a[href$='.7z']").addClass('rar');

	$('.project_gallery .thumbs').on('click', 'a' , function(e){
		e.preventDefault()
		var IMG = $('.project_gallery .big_img').find('img')
		var SRC = $(this).attr('href')
		$('.project_gallery .big_img').prepend('<div id="loader">loader</div>')
		IMG.hide()
		var ii = new Image()
		ii.src = SRC;
		ii.onload = function(){
			setTimeout(function() {
				$('#loader').remove()
				IMG.attr('src', SRC)
				IMG.show()
			}, 300) 
		}
		ii.src = SRC;
	})

	var ww = $('.slide_list__item').outerWidth(true)

	$('.slide_list').width($('.slide_list__item').size()*ww)
	$('.about_dev_project .close').on('click', function(){
		$(this).parent().fadeOut(400)
		$('.slide_list').animate({'left':0})
		$('.slide_btn.left').animate({'opacity':'.8'},100).addClass('inactive')
		$('.slide_btn.right').animate({'opacity':'1'},100).removeClass('inactive')
		list_pos = 0;
		ww = 160;
		$('.slide_list__item').css({'width':ww-1})
		var new_width = $('.slide_list__item').size()*(ww)
		console.log(new_width)
		$('.slide_list').css({'width':new_width})
		list_width = new_width;
		setTimeout(function(){
			$('.slide_wrapper').css({'width' : 960})
			.parent().addClass('rounded')
		}, 100)
	})

	var list_width=$('.slide_list').outerWidth(true),
		list_pos=($('.slide_list').position() ? $('.slide_list').position().left : 0),
		wrapper_width=$('.slide_wrapper').outerWidth(true)

	if(list_pos >= 0) $('.slide_btn.left').animate({'opacity':'.8'},100).addClass('inactive')
	if(list_width <= wrapper_width) $('.slide_btn.right').animate({'opacity':'.8'},100).addClass('inactive')
	$('.slide_btn.left').on('click', function(){
		if(list_pos >= 0) return $(this).animate({'opacity':'.8'},100).addClass('inactive')
			list_width=$('.slide_list').outerWidth(true);
			list_pos+=ww;
			wrapper_width=$('.slide_wrapper').outerWidth(true)
			$('.slide_list').animate({'left':'+=' + ww})
			$('.slide_btn.left').animate({'opacity':'1'},100).removeClass('inactive')
	})
	$('.slide_btn.right').on('click', function(){
		if((list_width > wrapper_width)&&(list_width + list_pos > wrapper_width)){
			list_width = $('.slide_list').outerWidth(true);
			list_pos = list_pos-ww;
			wrapper_width = $('.slide_wrapper').outerWidth(true)
			$('.slide_list').animate({'left':'-=' + ww})
			$('.slide_btn.left').animate({'opacity':'1'},100).removeClass('inactive')
		} else {
			return $(this).animate({'opacity':'.8'},100).addClass('inactive')
		}
	})

})