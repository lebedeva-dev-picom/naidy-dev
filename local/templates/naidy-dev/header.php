<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?php IncludeTemplateLangFile(__FILE__); ?>
<!DOCTYPE html PUBLIC>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html dir="ltr" lang="ru" class="no-js ie7 index"><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="ru" class="no-js ie8 index"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="ru" class="no-js ie9 index"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru" class="no-js index">
<!--<![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php $APPLICATION->ShowHead(); ?>
	<title><?php $APPLICATION->ShowTitle(); ?></title>

	<link href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin,cyrillic" rel="stylesheet" type="text/css">
	<link href="<?=SITE_TEMPLATE_PATH?>/css/style.css" type="text/css" rel="stylesheet" />

	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/scriptSlider.js"></script>
</head>

<?php $titlePage = '/' == $APPLICATION->GetCurPage(false); ?>

<body class="<?=($titlePage ? 'index' : 'inner')?>">
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="body_wrapper">
		<div class="topPanel wrap">
			<div class="topPanel inner">
				<?php $APPLICATION->IncludeFile(
					SITE_DIR."include/top_panel.php",
					Array(),
					Array("MODE" => "php")
				); ?>
			</div>
		<?php if ($titlePage): ?>
			<div class="borderBottom"></div>
		<?php endif; ?>
		</div>
		<div class="wrapper">
			<div class="header clr">
				<div class="logo fl_l">
					<a href="<?=SITE_DIR?>">
						<?php $APPLICATION->IncludeFile(
							SITE_DIR."include/company_name.php",
							Array(),
							Array("MODE" => "html")
						); ?>
					</a>
				</div>
				<div class="top_menu">
					<?php $APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"top_menu", 
						Array(
							"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
						),
						false,
						array("ACTIVE_COMPONENT" => "Y")
					); ?>
					<?php if (!$titlePage) {
						$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"second_menu", 
	array(
		"ROOT_MENU_TYPE" => "left",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "second_menu",
		"DELAY" => "N"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);
					} ?>
				</div> <!-- /top_menu__wrap -->
			</div> <!-- /header -->
			<div class="main">
			<?php if (!$titlePage): ?>
				<div class="head">
					<div class="title_block">
						<h1><?php $APPLICATION->ShowTitle(false); ?></h1>
						<?php $APPLICATION->IncludeComponent(
							"bitrix:breadcrumb",
							"breadcrumbs", Array(
								"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
								"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
								"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
							),
							false
						); ?>
					</div>
					<?php $APPLICATION->IncludeComponent(
						"bitrix:main.include", 
						".default", 
						array(
							"AREA_FILE_SHOW" => "page",
							"AREA_FILE_SUFFIX" => "include/header",
							"COMPONENT_TEMPLATE" => ".default",
							"EDIT_TEMPLATE" => ""
						),
						false
					); ?>
				</div> <!-- /head -->
			<?php endif; ?>
				<div class="content">