<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

$strReturn = '';

if (!empty($arResult)) {
	$strReturn .= '<ul class="breadcrumbs">';

	$itemSize = count($arResult);

	$arResult[$itemSize++]["TITLE"] = $APPLICATION->GetTitle();

	for($index = 0; $index < $itemSize; $index++)
	{
		$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

		$strReturn .= ($index > 0 ? '<li>/</li>' . chr(13) . chr(10) : '');
		$strReturn .= (!($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1)
			? '<li class="active">'
			: '<li>'
		);
		$strReturn .= '<a href="' . $arResult[$index]["LINK"] . '">' . $title . '</a>';
		$strReturn .= '</li>' . chr(13) . chr(10);
	}

	$strReturn .= '</ul>';
}
return $strReturn;