<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!-- <pre><?print_r($arResult)?></pre> -->

<div class="project_wrap">
	<h1><?=$arResult['NAME']?></h1>
	
	<div class="project_left fl_l">
		<div class="project_gallery">

			<div class="big_img">
				<div class="img">
					<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>"
						alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>"
						title="<?=$arResult['DETAIL_PICTURE']['TITLE']?>"
					/>
					<?php if('Y' != $arResult["PROPERTIES"]['completed']['VALUE']): ?>
						<div class="status inprogress"><i class="ico"></i>В работе</div>
					<?php else: ?>
						<div class="status"><i class="ico"></i>Завершен</div>
					<?php endif; ?>
				</div>
			</div>

			<ul class="thumbs clr">
			<?php foreach ($arResult['GALLERY_ITEMS'] as $value): ?>
				<li class="thumbs_item">
					<span class="img">
						<a href="<?=$value['PREVIEW_PICTURE']?>">
							<img src="<?=$value['PREVIEW_PICTURE']?>" 
								alt="<?=$value['NAME']?>"
								title="<?=$value['NAME']?>"
							/>
						</a>
					</span>
				</li>
			<?php endforeach; ?>
			</ul>

		</div> <!-- /project_gallery -->
	</div> <!-- /project_left -->

	<!-- PREVIEW_TEXT_SECTION -->
	<div class="project_right">
		<?=$arResult['PREVIEW_TEXT']?>
	</div> <!-- /project_right -->
	<!-- /PREVIEW_TEXT_SECTION -->

	<!-- MAIN_TEXT_SECTION -->
	<div class="project_text clr">
		<?=$arResult['DETAIL_TEXT']?>
	</div>
	<!-- MAIN_TEXT_SECTION -->
</div> <!-- /project_wrap -->