<?php

$arGalleryItems = Array();
$arGalleryItems[] = Array(
	'NAME' => $arResult['DETAIL_PICTURE']['TITLE'],
	'PREVIEW_PICTURE' => $arResult['DETAIL_PICTURE']['SRC']
	);
$res = CIBlockElement::GetList(
	Array('SORT' => 'ASC'),
	Array('ID' => $arResult['PROPERTIES']['gallery']['VALUE']),
	false,
	false,
	Array('IBLOCK_ID', 'ID', 'NAME', 'PREVIEW_PICTURE')
);
while ($obj = $res->GetNext()) {
	$arGalleryItems[] = Array(
		'NAME' => $obj['NAME'],
		'PREVIEW_PICTURE' => CFile::GetPath($obj['PREVIEW_PICTURE'])
	);
}
$arResult['GALLERY_ITEMS'] = $arGalleryItems;
unset($arGalleryItems);
