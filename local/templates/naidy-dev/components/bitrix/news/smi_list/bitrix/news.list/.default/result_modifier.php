<?php

foreach($arResult["ITEMS"] as &$arItem) {
	$res = CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID']);
	if ($ar_res = $res->GetNext()) {
		$arItem['SECTION_NAME'] = $ar_res['NAME'];
		$arItem['SECTION_PAGE_URL'] = $ar_res['SECTION_PAGE_URL'];
	}
}
