<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="title_wrap">
	<span class="dev_title">
		<a href="">
			<!--<span>Офисный</span><span>центр</span><span>«Билайн»</span>-->
		</a>
	</span>
</div>

<div class="slide_control">
	<div class="slide_wrapper">

		<ul class="slide_list clr">

<?php foreach($arResult["ITEMS"] as $arItem): ?>
	<?php
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li class="slide_list__item">
		<span class="container">
			<a href=""
				data-name="<?=$arItem['NAME']?>"
				data-link="<?=$arItem['DETAIL_PAGE_URL']?>"
				data-img="<?=$arResult['DETAIL_PICTURES'][$arItem['ID']]['SRC']?>"
			><?=$arItem['NAME']?></a>
		</span>
	</li>
<?php endforeach; ?>

		</ul>

	</div>

	<div class="slide_btn left">
		<i class="ico"></i>
	</div>
	<div class="slide_btn right">
		<i class="ico"></i>
	</div>

</div> <!-- .slide_control -->
