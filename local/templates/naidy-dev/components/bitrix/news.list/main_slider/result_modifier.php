<?php

$arItemId = Array();

foreach($arResult["ITEMS"] as $arItem) {
	$arItemId[] = $arItem['ID'];
}

if(CModule::IncludeModule("iblock"))
{
	$items = CIBlockElement::GetList(
		Array(),
		Array('ID' => $arItemId),
		false,
		false,
		Array('DETAIL_PICTURE', 'ID')
	);
}

$arBigPicture = Array();

while ($arItem = $items->GetNext()) {
	if (!empty($arItem["DETAIL_PICTURE"])) {
		$arBigPicture[$arItem['ID']] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
	}
}

$arResult['DETAIL_PICTURES'] = $arBigPicture;

unset($arBigPicture, $arItemId);
