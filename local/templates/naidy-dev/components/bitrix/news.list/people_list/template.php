<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if($arParams["DISPLAY_TOP_PAGER"]): ?>
	<?=$arResult["NAV_STRING"]?><br />
<?php endif; ?>

<div class="personal_cards people">
<?php foreach($arResult["ITEMS"] as $arItem): ?>
	<?php
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="profile">
		<div class="profile_image fl_l">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
				alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
				title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>" />
		</div>
		<div class="profile_content">
			<div class="name">
				<h2><?=$arItem["NAME"]?></h2>
			</div>
			<div class="profa">
				<?=$arItem["PREVIEW_TEXT"]?>
			</div>
			<div class="info">
				<p><?=$arItem["DETAIL_TEXT"]?></p>
			</div>
		</div>
	</div> <!-- /profile -->
<?php endforeach; ?>
</div>

<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<br /><?=$arResult["NAV_STRING"]?>
<?php endif; ?>