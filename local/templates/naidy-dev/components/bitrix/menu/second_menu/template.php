<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?php if (!empty($arResult)): ?>
	<div class="second_lvl_menu">
		<ul>
		<?php foreach($arResult as $arItem): ?>
			<li class="second_lvl_menu__item<?=($arItem["SELECTED"] ? ' active': '')?>">
				<a href="<?=$arItem["LINK"]?>">
					<?=$arItem["TEXT"]?>
				</a>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>