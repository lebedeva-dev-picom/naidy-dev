<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?php if (!empty($arResult)): ?>
		<div class="first_lvl_menu">
			<ul>
			<?php for ($i = 0; $i < count($arResult); $i++) {
				$class = 'first_lvl_menu__item';
				$class .= ($i == count($arResult) - 1 ? ' last' : '');
				$class .= ($arResult[$i]["SELECTED"] ? ' active' : '');
				?>
				<li>
					<span class="<?=$class?>">
						<span class="container">
							<a href="<?=$arResult[$i]["LINK"]?>">
								<?=$arResult[$i]["TEXT"]?>
							</a>
						</span>
					</span>
				</li>
			<?php } ?>
			</ul>
		</div>
<?php endif; ?>